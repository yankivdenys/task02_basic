
package com.epam.edu;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Contains some helper methods Mostly for arrays.
 */
final class Util {

  /**
   * Empty constructor.
   */
  private Util() {

  }

  /**
   *
   * Reads int from standard input safely.
   *
   * @param sc text scanner
   * @return user input
   */
  static int safeNextInt(final Scanner sc) {
    int input;
    while (true) {
      try {
        input = sc.nextInt();
        break;
      } catch (InputMismatchException e) {
        e.printStackTrace();
        System.out.println("Incorrect input.Try again:");
        sc.nextLine();
      }
    }
    return input;
  }

  /**
   *
   * Finds max number in array.
   *
   * @param array
   * @return
   */
  static int maxNumberInArray(final int[] array) {
    int max = array[0];
    for (int i = 1; i < array.length; i++) {
      if (array[i] > max) {
        max = array[i];
      }
    }

    return max;
  }

  /**
   *
   * Returns sub array of even numbers.
   *
   * @param array
   * @return
   */
  static int[] getEvenNumbers(final int[] array) {
    int count = 0;
    for (int item : array) {
      if (item % 2 == 0) {
        count++;
      }
    }
    if (count == 0) {
      return null;
    }
    int[] result = new int[count];
    int k = 0;
    for (int item : array) {
      if (item % 2 == 0) {
        result[k] = item;
        k++;
      }
    }
    return result;
  }

  /**
   *
   * Returns sub array of odd numbers.
   *
   * @param array
   * @return
   */
  static int[] getOddNumbers(final int[] array) {
    int count = 0;
    for (int item : array) {
      if (item % 2 != 0) {
        count++;
      }
    }
    if (count == 0) {
      return null;
    }
    int[] result = new int[count];
    int k = 0;
    for (int item : array) {

      if (item % 2 != 0) {
        result[k] = item;
        k++;
      }

    }
    return result;
  }

  /**
   *
   * Returns sum of elements from given array.
   *
   * @param array
   * @return
   */
  static int getSumOfElementsInArray(final int[] array) {
    int sum = 0;
    for (int item : array) {
      sum += item;
    }
    return sum;
  }

  /**
   *
   * Prints array from start to end.
   *
   * @param array
   */
  static void printArrayFromStart(final int[] array) {

    for (int item : array) {
      System.out.println(item);
    }
  }

  /**
   *
   * Prints array from end to start.
   *
   * @param array
   */
  static void printArrayFromEnd(final int[] array) {
    for (int i = array.length - 1; i >= 0; i--) {
      System.out.println(array[i]);
    }
  }

  /**
   * Returns percentage of some part.
   *
   * @param part
   * @param whole
   * @return
   */
  static double getPercentage(final int part, final int whole) {
    final int oneHundred = 100;
    return oneHundred * (double) part / whole;
  }
}
