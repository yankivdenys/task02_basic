package com.epam.edu;

/**
 * Contains main method.
 */
final class Task {

  /**
   * Empty constructor
   */
  private Task() {

  }

  /**
   * Executes tasks.
   *
   * @param args command line arguments
   */
  public static void main(final String[] args) {
    IntervalTask.executeTask();
    FibonacciTask.executeTask();
  }

}
